package com.recruitment.app.location.jpa.entity;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import static javax.persistence.GenerationType.IDENTITY;
import static lombok.AccessLevel.PRIVATE;

@Entity(name = "locations")
@Data
@NoArgsConstructor(access = PRIVATE, force = true)
@AllArgsConstructor(access = PRIVATE)
@Builder(toBuilder = true)
public class LocationEntity {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    Long id;
    @NonNull
    @Column(name = "device_id", nullable = false, updatable = false)
    final Long deviceId;
    @NonNull
    @Column(name = "longitude", nullable = false, updatable = false)
    final BigDecimal longitude;
    @NonNull
    @Column(name = "latitude", nullable = false, updatable = false)
    final BigDecimal latitude;
}
