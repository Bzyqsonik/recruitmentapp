package com.recruitment.app.location.jpa.repository;


import java.util.stream.Stream;

import com.recruitment.app.location.jpa.entity.LocationEntity;
import lombok.NonNull;
import org.springframework.data.repository.CrudRepository;

public interface LocationRepository extends CrudRepository<LocationEntity, Long> {
    Stream<LocationEntity> findAllByDeviceId(@NonNull final Long deviceId);
}
