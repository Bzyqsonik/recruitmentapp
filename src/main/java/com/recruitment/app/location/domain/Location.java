package com.recruitment.app.location.domain;

import java.math.BigDecimal;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;

import static lombok.AccessLevel.PRIVATE;

@Value
@Builder(toBuilder = true)
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
public class Location {
    @NonNull
    @Positive
    Long deviceId;
    @NonNull
    @Min(-180)
    @Max(180)
    BigDecimal longitude;
    @NonNull
    @Min(-180)
    @Max(180)
    BigDecimal latitude;
}
