package com.recruitment.app.location.domain;

import java.util.List;

import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class LocationsResponse {
    @NonNull
    List<Location> locations;
}
