package com.recruitment.app.location.controller;

import javax.validation.Valid;

import com.recruitment.app.location.domain.Location;
import com.recruitment.app.location.domain.LocationsResponse;
import com.recruitment.app.location.service.LocationService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/api/locations")
@AllArgsConstructor
public class LocationController {
    private final LocationService locationService;

    @PostMapping
    @ResponseStatus(NO_CONTENT)
    public void saveLocation(@RequestBody @Valid final Location location) {
        locationService.save(location);
    }

    @GetMapping
    public LocationsResponse findByDeviceId(@RequestParam final Long deviceId) {
        return LocationsResponse.of(locationService.findByDeviceId(deviceId));
    }
}
