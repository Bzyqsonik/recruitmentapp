package com.recruitment.app.location.service;

import java.util.List;
import java.util.function.Function;
import javax.transaction.Transactional;

import com.recruitment.app.location.domain.Location;
import com.recruitment.app.location.jpa.entity.LocationEntity;
import com.recruitment.app.location.jpa.repository.LocationRepository;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class LocationService {
    private final LocationRepository locationRepository;

    private static final Function<LocationEntity, Location> TO_DOMAIN = it -> Location.builder()
            .deviceId(it.getDeviceId())
            .longitude(it.getLongitude())
            .latitude(it.getLatitude())
            .build();

    @Transactional
    public List<Location> findByDeviceId(@NonNull final Long deviceId) {
        return locationRepository.findAllByDeviceId(deviceId)
                .map(TO_DOMAIN)
                .collect(toList());
    }

    public void save(@NonNull final Location location) {
        locationRepository.save(LocationEntity.builder()
                .deviceId(location.getDeviceId())
                .latitude(location.getLatitude())
                .longitude(location.getLongitude())
                .build());
    }
}
