package com.recruitment.app.tree;

import java.math.BigDecimal;
import java.util.Random;

import lombok.NonNull;

import static java.math.BigDecimal.ZERO;

public class RandomGrower {

    private static final Random GENERATOR = new Random();

    private RandomGrower() {}

    public static int getRandomGrowInt(@NonNull final Integer max) {
        return GENERATOR.nextInt(max);
    }

    public static BigDecimal getRandomGrowDecimal(@NonNull final Double max) {
        return GENERATOR
                .doubles(1, 0.0, max)
                .boxed()
                .findAny()
                .map(BigDecimal::valueOf)
                .orElse(ZERO);
    }
}
