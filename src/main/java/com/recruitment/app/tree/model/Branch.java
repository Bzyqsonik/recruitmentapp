package com.recruitment.app.tree.model;

public interface Branch {
    Branch getGrown();
}
