package com.recruitment.app.tree.model;

import lombok.Builder;
import lombok.Value;

import static com.recruitment.app.tree.RandomGrower.getRandomGrowInt;
import static lombok.AccessLevel.PRIVATE;

@Value
@Builder(toBuilder = true, access = PRIVATE)
public class NeedleBranch implements Branch {

    private static final int MAX_NEEDLES_PER_SEASON = 20;

    final int needlesNumber;

    static NeedleBranch create() {
        return NeedleBranch.builder()
                .needlesNumber(getRandomGrowInt(MAX_NEEDLES_PER_SEASON))
                .build();
    }

    @Override
    public Branch getGrown() {
        return toBuilder()
                .needlesNumber(needlesNumber + getRandomGrowInt(MAX_NEEDLES_PER_SEASON))
                .build();
    }
}
