package com.recruitment.app.tree.model;

import lombok.Builder;
import lombok.Value;

import static com.recruitment.app.tree.RandomGrower.getRandomGrowInt;
import static lombok.AccessLevel.PRIVATE;

@Value
@Builder(toBuilder = true, access = PRIVATE)
public class LeafyBranch implements Branch {

    private static final int MAX_LEAVES_PER_SEASON = 5;

    final int leavesNumber;

    static LeafyBranch create() {
        return LeafyBranch.builder()
                .leavesNumber(getRandomGrowInt(MAX_LEAVES_PER_SEASON))
                .build();
    }

    @Override
    public Branch getGrown() {
        return toBuilder()
                .leavesNumber(leavesNumber + getRandomGrowInt(MAX_LEAVES_PER_SEASON))
                .build();
    }
}
