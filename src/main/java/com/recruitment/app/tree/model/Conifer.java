package com.recruitment.app.tree.model;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

import static com.recruitment.app.tree.RandomGrower.getRandomGrowDecimal;
import static com.recruitment.app.tree.RandomGrower.getRandomGrowInt;
import static java.math.BigDecimal.ZERO;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static java.util.stream.Stream.concat;
import static lombok.AccessLevel.PRIVATE;

@EqualsAndHashCode(callSuper = true)
@Value
public class Conifer extends Tree {
    private static final double MAX_TRUNK_RADIUS_METERS_GROW_PER_SEASON = 0.03;
    private static final double MAX_HEIGHT_METERS_GROW_PER_SEASON = 0.3;

    @Builder(toBuilder = true, access = PRIVATE)
    private Conifer(final BigDecimal trunkRadius, final BigDecimal height, final int age, final List<Branch> branches) {
        super(branches, trunkRadius, height, age);
    }

    public static Conifer create() {
        return Conifer.builder()
                .age(0)
                .height(ZERO)
                .trunkRadius(ZERO)
                .branches(emptyList())
                .build();
    }

    @Override
    public Tree getGrown() {
        return toBuilder()
                .age(age + 1)
                .height(ZERO.add(getRandomGrowDecimal(MAX_HEIGHT_METERS_GROW_PER_SEASON)))
                .trunkRadius(ZERO.add(getRandomGrowDecimal(MAX_TRUNK_RADIUS_METERS_GROW_PER_SEASON)))
                .branches(
                        concat(
                                branches.stream().map(Branch::getGrown),
                                growNewBranches())
                                .collect(toList()))
                .build();
    }

    private Stream<NeedleBranch> growNewBranches() {
        return range(0, getRandomGrowInt(MAX_BRANCHES_PER_SEASON))
                .mapToObj(value -> NeedleBranch.create());
    }

    @Override
    public String toString() {
        return "Conifer{" +
                "branches=" + branches +
                ", trunkRadius=" + trunkRadius +
                ", height=" + height +
                ", age=" + age +
                '}';
    }
}
