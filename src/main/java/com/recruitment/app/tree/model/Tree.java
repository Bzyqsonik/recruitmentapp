package com.recruitment.app.tree.model;

import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.FieldDefaults;

import static lombok.AccessLevel.PROTECTED;

@Data
@FieldDefaults(makeFinal = true, level = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public abstract class Tree {
    static final int MAX_BRANCHES_PER_SEASON = 5;

    @NonNull
    List<Branch> branches;
    @NonNull
    BigDecimal trunkRadius;
    @NonNull
    BigDecimal height;
    @NonNull
    Integer age;

    public abstract Tree getGrown();
}
