package com.recruitment.app.tree;

import java.util.stream.Stream;

import com.recruitment.app.tree.model.Conifer;
import com.recruitment.app.tree.model.LeafyTree;
import com.recruitment.app.tree.model.Tree;

public class TreeApp {

    public static void main(String[] args) {
        Stream.of(
                LeafyTree.create(),
                LeafyTree.create(),
                Conifer.create(),
                Conifer.create()
        )
                .map(Tree::getGrown)
                .map(Tree::getGrown)
                .map(Tree::getGrown)
                .forEach(System.out::println);
    }
}
