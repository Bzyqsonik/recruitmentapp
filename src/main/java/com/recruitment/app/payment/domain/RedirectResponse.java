package com.recruitment.app.payment.domain;

import lombok.Value;

@Value(staticConstructor = "of")
public class RedirectResponse {
    String redirectUri;
}
