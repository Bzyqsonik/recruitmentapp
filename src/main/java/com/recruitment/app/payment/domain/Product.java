package com.recruitment.app.payment.domain;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;

import static lombok.AccessLevel.PRIVATE;

@Value
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
@Builder
public class Product {
    @NonNull
    String name;
    @NonNull
    BigDecimal unitPrice;
    @NonNull
    Integer quantity;
}
