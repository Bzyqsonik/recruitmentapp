package com.recruitment.app.payment.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;

import static lombok.AccessLevel.PRIVATE;

@Value
@AllArgsConstructor(access = PRIVATE)
@NoArgsConstructor(access = PRIVATE, force = true)
@Builder
public class Order {
    @NonNull
    String description;
    @NonNull
    String currencyCode;
    @NonNull
    List<Product> products;
}
