package com.recruitment.app.payment.domain;

import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class OrderResponse {
    @NonNull
    String orderId;
}
