package com.recruitment.app.payment.service;

import com.recruitment.app.payment.domain.Order;
import com.recruitment.app.payment.mapper.PayUOrderMapper;
import com.recruitment.app.payment.payu.client.PayUOrderClient;
import com.recruitment.app.payment.payu.model.PayUOrder;
import com.recruitment.app.payment.payu.model.PayUOrderResponse;
import com.recruitment.app.payment.payu.service.PayUTokenService;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
@AllArgsConstructor
public class PaymentService {
    private static final String AUTHORIZATION_HEADER_PATTERN = "Bearer %s";

    private final PayUTokenService payUTokenService;
    private final PayUOrderClient payUOrderClient;
    private final PayUOrderMapper payUOrderMapper;

    public String makeOrder(@NonNull final String customerIp,
                            @NonNull final Order order) {
        final PayUOrder payUOrder = payUOrderMapper.apply(customerIp, order);
        final PayUOrderResponse payUOrderResponse = payUOrderClient.makeOrder(getAuthorizationHeader(), payUOrder);
        return payUOrderResponse.getOrderId();
    }

    private String getAuthorizationHeader() {
        return format(AUTHORIZATION_HEADER_PATTERN, payUTokenService.getToken());
    }
}
