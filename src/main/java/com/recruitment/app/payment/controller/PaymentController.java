package com.recruitment.app.payment.controller;

import com.recruitment.app.payment.domain.Order;
import com.recruitment.app.payment.domain.OrderResponse;
import com.recruitment.app.payment.domain.RedirectResponse;
import com.recruitment.app.payment.payu.exception.RedirectException;
import com.recruitment.app.payment.service.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.FOUND;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api/payments")
@AllArgsConstructor
public class PaymentController {

    private final PaymentService paymentService;

    @PostMapping
    public OrderResponse pay(@RequestHeader(value = "X-User-IP") final String customerIp,
                             @RequestBody final Order order) {
        return OrderResponse.of(paymentService.makeOrder(customerIp, order));
    }

    @ExceptionHandler
    public ResponseEntity<RedirectResponse> handleRedirectException(final RedirectException ex) {
        return status(FOUND)
                .body(RedirectResponse.of(ex.getRedirectLocation()));
    }
}
