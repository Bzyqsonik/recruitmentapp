package com.recruitment.app.payment.mapper;

import java.math.BigDecimal;

import com.recruitment.app.payment.domain.Order;
import com.recruitment.app.payment.domain.Product;
import com.recruitment.app.payment.payu.config.PayUAuthClientConfiguration;
import com.recruitment.app.payment.payu.model.PayUOrder;
import com.recruitment.app.payment.payu.model.PayUProduct;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.stereotype.Component;

import static java.math.RoundingMode.HALF_EVEN;
import static java.util.stream.Collectors.toList;

@Component
@AllArgsConstructor
public class PayUOrderMapper {
    private final PayUAuthClientConfiguration payUAuthClientConfiguration;

    private static final String HUNGARIAN_CURRENCY_CODE = "HUF";

    public PayUOrder apply(@NonNull final String customerIp, @NonNull final Order order) {
        final String currencyCode = order.getCurrencyCode();
        return PayUOrder.builder()
                .customerIp(customerIp)
                .merchantPosId(payUAuthClientConfiguration.getClientId())
                .currencyCode(currencyCode)
                .description(order.getDescription())
                .totalAmount(getTotalAmount(order))
                .products(order.getProducts()
                        .stream()
                        .map(it -> toPayUProduct(it, currencyCode))
                        .collect(toList()))
                .build();
    }

    private static String getTotalAmount(final Order order) {
        return order.getProducts()
                .stream()
                .map(it -> it.getUnitPrice().multiply(BigDecimal.valueOf(it.getQuantity())))
                .reduce(BigDecimal::add)
                .map(it -> getPriceCents(it, order.getCurrencyCode()))
                .orElse("0");
    }

    private static PayUProduct toPayUProduct(final Product product, final String currencyCode) {
        return PayUProduct.builder()
                .name(product.getName())
                .quantity(product.getQuantity().toString())
                .unitPrice(getPriceCents(product.getUnitPrice(), currencyCode))
                .build();
    }

    private static String getPriceCents(final BigDecimal price, final String currencyCode) {
        final int scale = HUNGARIAN_CURRENCY_CODE.equals(currencyCode) ? 3 : 2;
        return price.setScale(scale, HALF_EVEN).movePointRight(scale).toString();
    }
}
