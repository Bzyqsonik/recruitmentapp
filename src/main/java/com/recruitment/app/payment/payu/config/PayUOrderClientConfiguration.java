package com.recruitment.app.payment.payu.config;

import java.io.IOException;

import com.recruitment.app.payment.payu.exception.RedirectException;
import feign.Client;
import feign.Request;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PayUOrderClientConfiguration {

    @Bean
    Client client() {
        return new NoRedirectsClient();
    }

    //Ugly solution but it was hard to force feign to stop treating 302 response as non-error status
    @Bean
    ErrorDecoder errorDecoder() {
        return (methodKey, response) -> new RedirectException(response.headers().get("location").stream().findAny().orElse(null));
    }
}

class NoRedirectsClient extends Client.Default {

    NoRedirectsClient() {
        super(null, null);
    }

    @Override
    public Response execute(final Request request, final Request.Options options) throws IOException {
        return super.execute(request, new Request.Options(10000, 30000, false));
    }
}
