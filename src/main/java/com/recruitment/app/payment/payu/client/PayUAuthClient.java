package com.recruitment.app.payment.payu.client;

import java.util.Map;

import com.recruitment.app.payment.payu.model.PayUAuth;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import lombok.NonNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PostMapping;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;

@FeignClient(name = "Pay-UAuthClient",
             url = "${external.service.pay-u.auth.url}",
             configuration = PayUAuthClient.EncodedFormConfiguration.class)
public interface PayUAuthClient {

    @PostMapping(consumes = APPLICATION_FORM_URLENCODED_VALUE)
    PayUAuth obtainToken(@NonNull final Map<String, ?> form);

    class EncodedFormConfiguration {

        @Bean
        Encoder feignFormEncoder(ObjectFactory<HttpMessageConverters> converters) {
            return new SpringFormEncoder(new SpringEncoder(converters));
        }
    }
}
