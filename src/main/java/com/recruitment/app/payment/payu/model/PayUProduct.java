package com.recruitment.app.payment.payu.model;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class PayUProduct {
    @NonNull
    String name;
    @NonNull
    String unitPrice;
    @NonNull
    String quantity;
}
