package com.recruitment.app.payment.payu.service;

import java.util.HashMap;
import java.util.Map;

import com.recruitment.app.payment.payu.client.PayUAuthClient;
import com.recruitment.app.payment.payu.config.PayUAuthClientConfiguration;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PayUTokenService {
    private final PayUAuthClient payUAuthClient;
    private final PayUAuthClientConfiguration payUAuthClientConfiguration;

    @Cacheable("PayUToken")
    public String getToken() {
        return payUAuthClient.obtainToken(buildForm()).getToken();
    }

    private Map<String, String> buildForm() {
        final Map<String, String> form = new HashMap<>();
        form.put("grant_type", "client_credentials");
        form.put("client_id", payUAuthClientConfiguration.getClientId());
        form.put("client_secret", payUAuthClientConfiguration.getClientSecret());
        return form;
    }

}
