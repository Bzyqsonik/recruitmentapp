package com.recruitment.app.payment.payu.exception;


import lombok.Getter;

public class RedirectException extends RuntimeException {
    @Getter
    private final String redirectLocation;

    public RedirectException(final String redirectLocation) {
        super();
        this.redirectLocation = redirectLocation;
    }
}
