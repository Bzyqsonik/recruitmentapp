package com.recruitment.app.payment.payu.model;

import java.util.List;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
public class PayUOrder {
    @NonNull
    String customerIp;
    @NonNull
    String merchantPosId;
    @NonNull
    String description;
    @NonNull
    String currencyCode;
    @NonNull
    String totalAmount;
    @NonNull
    List<PayUProduct> products;
}
