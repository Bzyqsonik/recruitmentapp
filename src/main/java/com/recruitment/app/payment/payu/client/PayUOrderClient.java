package com.recruitment.app.payment.payu.client;

import com.recruitment.app.payment.payu.config.PayUOrderClientConfiguration;
import com.recruitment.app.payment.payu.model.PayUOrder;
import com.recruitment.app.payment.payu.model.PayUOrderResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "Pay-UOrderClient",
             url = "${external.service.pay-u.order.url}",
             configuration = PayUOrderClientConfiguration.class)
public interface PayUOrderClient {

    @PostMapping
    PayUOrderResponse makeOrder(@RequestHeader("Authorization") final String authHeader,
                                @RequestBody final PayUOrder payUOrder);
}
