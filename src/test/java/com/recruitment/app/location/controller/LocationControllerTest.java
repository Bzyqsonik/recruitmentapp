package com.recruitment.app.location.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.recruitment.app.location.domain.Location;
import com.recruitment.app.location.domain.LocationsResponse;
import com.recruitment.app.location.service.LocationService;
import lombok.Value;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;

import static java.math.BigDecimal.valueOf;
import static java.util.stream.Collectors.toList;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(MockitoJUnitRunner.class)
public class LocationControllerTest {

    private static final Fixtures fixtures = new Fixtures();

    @Mock
    private LocationService locationService;
    @InjectMocks
    private LocationController locationController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = standaloneSetup(locationController)
                .build();
    }

    @Test
    public void shouldSaveNewLocation() throws Exception {
        //when
        mockMvc.perform(
                post("/api/locations")
                        .contentType(APPLICATION_JSON)
                        .content(fixtures.firstLocationJson))
                //then
                .andExpect(status().is(NO_CONTENT.value()))
                .andDo(result -> verify(locationService).save(fixtures.firstLocation));
    }

    @Test
    public void shouldReturnBadRequestForWrongDeviceId() throws Exception {
        //when
        mockMvc.perform(
                post("/api/locations")
                        .contentType(APPLICATION_JSON)
                        .content(fixtures.wrongDeviceIdLocationJson))
                //then
                .andExpect(status().is(BAD_REQUEST.value()))
                .andDo(result -> verifyZeroInteractions(locationService));
    }

    @Test
    public void shouldReturnBadRequestForWrongLongitude() throws Exception {
        //when
        mockMvc.perform(
                post("/api/locations")
                        .contentType(APPLICATION_JSON)
                        .content(fixtures.wrongLongitudeLocationJson))
                //then
                .andExpect(status().is(BAD_REQUEST.value()))
                .andDo(result -> verifyZeroInteractions(locationService));
    }

    @Test
    public void shouldReturnBadRequestForWrongLatitude() throws Exception {
        //when
        mockMvc.perform(
                post("/api/locations")
                        .contentType(APPLICATION_JSON)
                        .content(fixtures.wrongLatitudeLocationJson))
                //then
                .andExpect(status().is(BAD_REQUEST.value()))
                .andDo(result -> verifyZeroInteractions(locationService));
    }

    @Test
    public void shouldReturnAnAgentById() throws Exception {
        //given
        given(locationService.findByDeviceId(fixtures.deviceId)).willReturn(fixtures.locations);

        //when
        mockMvc.perform(get("/api/locations").param("deviceId", fixtures.deviceId.toString()))
                //then
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON))
                .andExpect(content().json(fixtures.locationsResponseJson));
    }

    @Value
    private static class Fixtures {
        ObjectMapper objectMapper = new ObjectMapper();

        Long deviceId = 1L;
        Long wrongDeviceId = 0L;
        BigDecimal longitude = valueOf(178.9);
        BigDecimal latitude = valueOf(-77);
        BigDecimal secondLongitude = valueOf(0);
        BigDecimal secondLatitude = valueOf(-111.3);
        BigDecimal wrongCoordinate = valueOf(-200);

        Location firstLocation = Location.builder()
                .deviceId(deviceId)
                .longitude(longitude)
                .latitude(latitude)
                .build();
        Location secondLocation = firstLocation.toBuilder()
                .longitude(secondLongitude)
                .latitude(secondLatitude)
                .build();

        Location wrongDeviceIdLocation = firstLocation.toBuilder()
                .deviceId(wrongDeviceId)
                .build();
        Location wrongLongitudeLocation = firstLocation.toBuilder()
                .longitude(wrongCoordinate)
                .build();
        Location wrongLatitudeLocation = firstLocation.toBuilder()
                .latitude(wrongCoordinate)
                .build();

        List<Location> locations = Stream.of(firstLocation, secondLocation).collect(toList());

        LocationsResponse locationsResponse = LocationsResponse.of(locations);

        String locationsResponseJson = toJson(locationsResponse);
        String firstLocationJson = toJson(firstLocation);
        String wrongDeviceIdLocationJson = toJson(wrongDeviceIdLocation);
        String wrongLongitudeLocationJson = toJson(wrongLongitudeLocation);
        String wrongLatitudeLocationJson = toJson(wrongLatitudeLocation);

        private String toJson(final Object object) {
            try {
                return objectMapper.writeValueAsString(object);
            } catch (final JsonProcessingException e) {
                e.printStackTrace();
                return "";
            }
        }
    }
}