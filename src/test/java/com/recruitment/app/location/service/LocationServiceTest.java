package com.recruitment.app.location.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import com.recruitment.app.location.domain.Location;
import com.recruitment.app.location.jpa.entity.LocationEntity;
import com.recruitment.app.location.jpa.repository.LocationRepository;
import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.math.BigDecimal.valueOf;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Stream.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LocationServiceTest {
    private static final Fixtures fixtures = new Fixtures();

    @Mock
    private LocationRepository locationRepository;
    @InjectMocks
    private LocationService locationService;

    @Test
    public void shouldFindLocationsByDeviceId() {
        //given
        given(locationRepository.findAllByDeviceId(fixtures.deviceId)).willReturn(Stream.of(fixtures.locationEntity));

        //when
        final List<Location> locations = locationService.findByDeviceId(fixtures.deviceId);

        //then
        assertThat(locations).containsExactlyInAnyOrderElementsOf(fixtures.foundLocations);
    }

    @Test
    public void shouldReturnEmptyList() {
        //given
        given(locationRepository.findAllByDeviceId(fixtures.deviceId)).willReturn(empty());

        //when
        final List<Location> locations = locationService.findByDeviceId(fixtures.deviceId);

        //then
        assertThat(locations).isEmpty();
    }

    @Test
    public void shouldSaveLocation() {
        //when
        locationService.save(fixtures.location);

        //then
        verify(locationRepository).save(fixtures.locationEntity);
    }

    @Value
    private static class Fixtures {
        Long deviceId = 1L;
        BigDecimal longitude = valueOf(-36.7);
        BigDecimal latitude = valueOf(77.7);

        LocationEntity locationEntity = LocationEntity.builder()
                .deviceId(deviceId)
                .longitude(longitude)
                .latitude(latitude)
                .build();

        Location location = Location.builder()
                .deviceId(deviceId)
                .longitude(longitude)
                .latitude(latitude)
                .build();

        List<Location> foundLocations = Stream.of(location).collect(toList());
    }
}