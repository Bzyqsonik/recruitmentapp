package com.recruitment.app.payment.payu.service;

import java.util.HashMap;
import java.util.Map;

import com.recruitment.app.payment.payu.client.PayUAuthClient;
import com.recruitment.app.payment.payu.config.PayUAuthClientConfiguration;
import com.recruitment.app.payment.payu.model.PayUAuth;
import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PayUTokenServiceTest {

    private static final Fixtures fixtures = new Fixtures();

    @Mock
    private PayUAuthClient payUAuthClient;
    @Mock
    private PayUAuthClientConfiguration payUAuthClientConfiguration;
    @InjectMocks
    private PayUTokenService payUTokenService;

    @Test
    public void shouldGetToken() {
        //given
        given(payUAuthClientConfiguration.getClientId()).willReturn(fixtures.clientId);
        given(payUAuthClientConfiguration.getClientSecret()).willReturn(fixtures.clientSecret);
        given(payUAuthClient.obtainToken(fixtures.form)).willReturn(fixtures.payUAuth);

        //when
        final String token = payUTokenService.getToken();

        //then
        assertThat(token).isEqualTo(fixtures.token);
    }

    @Value
    private static class Fixtures {
        String clientId = "12345";
        String clientSecret = "ptaki lataja kluczem";
        String token = "token";

        Map<String, String> form = form();

        PayUAuth payUAuth = PayUAuth.builder()
                .token(token)
                .build();

        private Map<String, String> form() {
            final Map<String, String> form = new HashMap<>();
            form.put("grant_type", "client_credentials");
            form.put("client_id", clientId);
            form.put("client_secret", clientSecret);
            return form;
        }
    }
}