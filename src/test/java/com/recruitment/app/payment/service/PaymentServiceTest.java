package com.recruitment.app.payment.service;

import java.math.BigDecimal;
import java.util.stream.Stream;

import com.recruitment.app.payment.domain.Order;
import com.recruitment.app.payment.domain.Product;
import com.recruitment.app.payment.mapper.PayUOrderMapper;
import com.recruitment.app.payment.payu.client.PayUOrderClient;
import com.recruitment.app.payment.payu.model.PayUOrder;
import com.recruitment.app.payment.payu.model.PayUOrderResponse;
import com.recruitment.app.payment.payu.model.PayUOrderResponseStatus;
import com.recruitment.app.payment.payu.model.PayUProduct;
import com.recruitment.app.payment.payu.service.PayUTokenService;
import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.math.BigDecimal.valueOf;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PaymentServiceTest {

    private static final Fixtures fixtures = new Fixtures();

    @Mock
    private PayUTokenService payUTokenService;
    @Mock
    private PayUOrderClient payUOrderClient;
    @Mock
    private PayUOrderMapper payUOrderMapper;
    @InjectMocks
    private PaymentService paymentService;

    @Test
    public void shouldMakePaymentAndReturnOrd() {
        //given
        given(payUTokenService.getToken()).willReturn(fixtures.token);
        given(payUOrderMapper.apply(fixtures.customerIp, fixtures.order)).willReturn(fixtures.payUOrder);
        given(payUOrderClient.makeOrder(fixtures.authHeader, fixtures.payUOrder)).willReturn(fixtures.payUOrderResponse);

        //when
        final String orderId = paymentService.makeOrder(fixtures.customerIp, fixtures.order);

        //then
        assertThat(orderId).isEqualTo(fixtures.orderId);
    }

    @Value
    private static class Fixtures {
        String token = "qwertyuiop";
        String authHeader = "Bearer " + token;
        String clientId = "12345";
        String customerIp = "127.0.0.1";
        String description = "Transakcja";
        String currencyCode = "PLN";
        String totalAmount = "4999";
        String cupProductName = "Kubek";
        BigDecimal cupPrice = valueOf(49.99);
        String cupPayUPrice = "4999";
        String orderId = "orderId";
        String successStatusCode = "SUCCESS";

        Product productCup = Product.builder()
                .quantity(1)
                .name(cupProductName)
                .unitPrice(cupPrice)
                .build();

        Order order = Order.builder()
                .currencyCode(currencyCode)
                .description(description)
                .products(Stream.of(productCup).collect(toList()))
                .build();

        PayUProduct payUProductCup = PayUProduct.builder()
                .name(cupProductName)
                .quantity("1")
                .unitPrice(cupPayUPrice)
                .build();

        PayUOrder payUOrder = PayUOrder.builder()
                .merchantPosId(clientId)
                .customerIp(customerIp)
                .description(description)
                .totalAmount(totalAmount)
                .currencyCode(currencyCode)
                .products(Stream.of(payUProductCup).collect(toList()))
                .build();

        PayUOrderResponseStatus payUOrderResponseStatus = PayUOrderResponseStatus.builder()
                .statusCode(successStatusCode)
                .build();

        PayUOrderResponse payUOrderResponse = PayUOrderResponse.builder()
                .orderId(orderId)
                .status(payUOrderResponseStatus)
                .build();
    }
}