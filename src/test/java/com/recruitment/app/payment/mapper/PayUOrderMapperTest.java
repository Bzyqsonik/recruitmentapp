package com.recruitment.app.payment.mapper;

import java.math.BigDecimal;
import java.util.stream.Stream;

import com.recruitment.app.payment.domain.Order;
import com.recruitment.app.payment.domain.Product;
import com.recruitment.app.payment.payu.config.PayUAuthClientConfiguration;
import com.recruitment.app.payment.payu.model.PayUOrder;
import com.recruitment.app.payment.payu.model.PayUProduct;
import lombok.Value;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static java.math.BigDecimal.valueOf;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class PayUOrderMapperTest {

    private static Fixtures fixtures = new Fixtures();

    @Mock
    private PayUAuthClientConfiguration payUAuthClientConfiguration;
    @InjectMocks
    private PayUOrderMapper payUOrderMapper;

    @Test
    public void shouldMapOrderToPayUOrder() {
        //given
        given(payUAuthClientConfiguration.getClientId()).willReturn(fixtures.clientId);

        //when
        final PayUOrder payUOrder = payUOrderMapper.apply(fixtures.customerIp, fixtures.order);

        //then
        assertThat(payUOrder).isEqualTo(fixtures.payUOrder);
    }

    @Value
    private static class Fixtures {
        String clientId = "12345";
        String customerIp = "127.0.0.1";
        String description = "Transakcja";
        String currencyCode = "PLN";
        String totalAmount = "9999";
        String cupProductName = "Kubek";
        String spoonProductName = "Lyzka";
        BigDecimal cupPrice = valueOf(49.99);
        String cupPayUPrice = "4999";
        BigDecimal spoonPrice = valueOf(10);
        String spoonPayUPrice = "1000";

        Product productCup = Product.builder()
                .quantity(1)
                .name(cupProductName)
                .unitPrice(cupPrice)
                .build();

        Product productSpoon = Product.builder()
                .quantity(5)
                .name(spoonProductName)
                .unitPrice(spoonPrice)
                .build();

        Order order = Order.builder()
                .currencyCode(currencyCode)
                .description(description)
                .products(Stream.of(productCup, productSpoon).collect(toList()))
                .build();

        PayUProduct payUProductCup = PayUProduct.builder()
                .name(cupProductName)
                .quantity("1")
                .unitPrice(cupPayUPrice)
                .build();

        PayUProduct payUProductSpoonX5 = PayUProduct.builder()
                .name(spoonProductName)
                .quantity("5")
                .unitPrice(spoonPayUPrice)
                .build();

        PayUOrder payUOrder = PayUOrder.builder()
                .merchantPosId(clientId)
                .customerIp(customerIp)
                .description(description)
                .totalAmount(totalAmount)
                .currencyCode(currencyCode)
                .products(Stream.of(payUProductCup, payUProductSpoonX5).collect(toList()))
                .build();
    }
}