### 1. Tree
Whole tree-object-model and tree-microapplication is based in package **tree**

### 2. Location
Whole logic is based in package **location**

Requests:

**POST**(http://localhost:8080/api/locations) - RequestBody:

{
    
    "deviceId": 12345,
    "latitude": 179.65, //MIN(-180), MAX(180),
    "longitude": -65 //MIN(-180), MAX(180)
    
}    
 
**GET**(http://localhost:8080/api/locations?deviceId=someNumber})

### 3. PayU
Whole logic is based in package **payment**

Request: **POST**(http://localhost:8080/api/payments) - RequestBody:


{

	"description": "Transakcja",
	"currencyCode": "PLN",
	"products": [
		{
			"name": "Kubek",
			"unitPrice": 25,
			"quantity": 2
		},
		{
			"name": "Talerz",
			"unitPrice": 10,
			"quantity": 5
		}
	]

}

Response: redirection-url to payU payment further step

Both location and payU logic are ran by one Springboot application - **RecruitmentAppApplication**

